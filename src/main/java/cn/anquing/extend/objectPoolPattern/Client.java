package cn.anquing.extend.objectPoolPattern;

import org.apache.commons.pool2.impl.GenericObjectPool;

import java.io.*;


public class Client {

    public static void main(String[] args) throws IOException {

        ReaderUtil readerUtil = new ReaderUtil(new GenericObjectPool<StringBuffer>(new StringBufferFactory()));

        FileInputStream fileInputStream = new FileInputStream("D:\\testout.sql");

        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);


        String s = readerUtil.readToString(inputStreamReader);

        System.out.println(s);

    }
}
