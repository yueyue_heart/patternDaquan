package cn.anquing.util;

import java.io.*;

/**
 * 需要克隆对象必须要实现serializable接口，通过对对象的序列化和反序列化实现克隆，可达到的真正深度克隆
 * 通过泛型限定，可以在编译时发现异常，而Object类的clone方法，只能在运行时发现异常
 */

public class CloneUtil {

    /*
    将构造方法私有化
     */
    private CloneUtil() {
        throw new AssertionError();
    }

    public static <T> T clone(T obj) throws Exception{
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bout);
        oos.writeObject(obj);

        ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bin);
        return (T)ois.readObject();

        //说明：调用ByteArrayOutputStream或ByteArrayInputStream对象的close方法没有任何意义
        //这两个基于内存的流只要垃圾回收器清理对象就能够释资源，这一点不同于对外部资源（如文件流）的释放
    }
}
