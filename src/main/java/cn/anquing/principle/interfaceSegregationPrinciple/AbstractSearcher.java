package cn.anquing.principle.interfaceSegregationPrinciple;


/**
 * 我是一个只气质的星探
 */
public abstract class AbstractSearcher {

    protected IGreatTemperamentGirl gretTemperamentGirl;


    public AbstractSearcher(IGreatTemperamentGirl _gretTemperamentGirl){
        this.gretTemperamentGirl = _gretTemperamentGirl;

    }

    //搜索美女，列出美女信息
    public abstract void show();
}
