package cn.anquing.principle.interfaceSegregationPrinciple;

public interface IGoodBodyGirl {

    //要有姣好的面孔
    void goodLooking();

    //要有好身材
    void niceFigure();

}
