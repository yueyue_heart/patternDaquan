package cn.anquing.principle.interfaceSegregationPrinciple;

/**
 * 接口隔离原则
 第一种定义: Clients should not be forced to depend upon interfaces that they don't use.
 客户端不应该依赖它不需用的接口。
 第二种定义：The dependency of one class to another one should depend on the smallest possible interface。
 类间的依赖关系应该建立在最小的接口上。
 */

/**
 *接口隔离原则是对接口进行规范约束，其包含以下四层含义：
 * 接口尽量要小，但根据接口隔离原则拆分接口时，必须首先满足单一职责原则
 * 接口要高内聚，具体到接口隔离原则就是要求在接口中尽量少公布 public 方法，接口是对外的承诺，承诺越少对系统的开发越有利，变更的风险也就越少，同时也有利于降低成本。
 * 定制服务，一个系统或系统内的模块之间必然会有耦合，有耦合就要相互访问的接口，我们设计时就需要给各个访问者定制服务。
 * 接口设计是有限度的:
 * 一个接口只服务于一个子模块或者业务逻辑。
 * 通过业务逻辑压缩接口中的 public 方法。
 * 已经被污染了的接口，尽量去修改，若变更的风险较大，则采用适配器模式进行转化处理。
 * 了解环境，拒绝盲从。
 * 怎么准确的实践接口隔离原则？一句话：实践，经验和领悟！
 */

public class Client {

    //搜索并展示美女信息
    public static void main(String[] args) {

        //定义一个美女
        PettyGirl yanYan = new PettyGirl("嫣嫣");


        AbstractSearcher searcher2 = new SearcherMM((IGreatTemperamentGirl) yanYan);


        searcher2.show();

    }
}
