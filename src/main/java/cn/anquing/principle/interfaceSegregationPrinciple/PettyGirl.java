package cn.anquing.principle.interfaceSegregationPrinciple;

public class PettyGirl implements IGoodBodyGirl,IGreatTemperamentGirl {

    //美女都有名字
    private String name;

    public PettyGirl(String name) {
        this.name = name;
    }

    //脸蛋漂亮
    @Override
    public void goodLooking() {
        System.out.println(this.name + "---脸蛋很漂亮!");
    }

    //气质要好
    @Override
    public void greatTemperament() {
        System.out.println(this.name + "---气质非常好!");
    }
    //身材要好
    @Override
    public void niceFigure() {
        System.out.println(this.name + "---身材非常棒!");
    }
}
