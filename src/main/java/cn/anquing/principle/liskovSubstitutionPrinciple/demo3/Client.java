package cn.anquing.principle.liskovSubstitutionPrinciple.demo3;

import java.util.HashMap;

public class Client {

    public static void invoker1(){
        //父类存在的地方，子类就应该能够存在
        Father f = new Father();
        HashMap map = new HashMap();
        f.doSomething(map);
    }

    public static void invoker2(){
        //父类存在的地方，子类就应该能够存在
        Son f =new Son();
        HashMap map = new HashMap();
        f.doSomething(map);
    }

    public static void main(String[] args) {

        //运行结果:
        //父类被执行...
        //父类被执行...
        invoker1();
        invoker2();
    }
}
