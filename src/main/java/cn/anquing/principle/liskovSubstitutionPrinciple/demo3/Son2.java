package cn.anquing.principle.liskovSubstitutionPrinciple.demo3;

import java.util.Collection;
import java.util.HashMap;

public class Son2 extends Father {

    //缩小输入参数范围（会重写）
    @Override
    public Collection doSomething(HashMap map){

        System.out.println("HashMap转Collection被执行...");
        return map.values();
    }
}
