package cn.anquing.principle.liskovSubstitutionPrinciple.demo3;

import java.util.Collection;
import java.util.Map;


/**
 * 大家注意看黄色明显标记部分，和父类同样的一个方法名称，但是又不是重写（Override）父类的方
 法，你加个@Override 试试看，报错的，为什么呢？是输入参数类型不同，编译器就不认为是重写父类的方
 法了，那这是什么？是重载（Overload）！不用大惊小怪的，不在一个类就不能是重载了？
 */
public class Son extends Father {

    //放大输入参数类型（会重载，不会）
    public Collection doSomething(Map map){//黄色标记
        System.out.println("子类被执行...");
        return map.values();
    }
}
