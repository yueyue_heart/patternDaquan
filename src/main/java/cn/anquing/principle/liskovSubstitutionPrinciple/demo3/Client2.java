package cn.anquing.principle.liskovSubstitutionPrinciple.demo3;

import java.util.HashMap;

public class Client2 {

    public static void invoker1(){

        //有父类的地方就有子类
        Father2 f= new Father2();
        HashMap map = new HashMap();
        f.doSomething(map);
    }

    public static void invoker2(){
        //有父类的地方就有子类
        Son2 s =new Son2();
        HashMap map = new HashMap();
        s.doSomething(map);
    }

    public static void main(String[] args) {
        invoker1();
        invoker2();
    }

}
