package cn.anquing.principle.liskovSubstitutionPrinciple.demo4;

import java.util.*;


/**
 * 大家注意看黄色明显标记部分，和父类同样的一个方法名称，输入参数类型相同，返回类型为原返回类型的子类型，属于重写
 */
public class Son extends Father {

    //输出结果被缩小（重写）
    @Override
    public List doSomething(HashMap map){//黄色标记
        System.out.println("子类被执行...");
        return new ArrayList();
    }
}
