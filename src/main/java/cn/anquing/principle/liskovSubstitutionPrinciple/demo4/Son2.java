package cn.anquing.principle.liskovSubstitutionPrinciple.demo4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;



public class Son2 extends Father {

    //输出结果被扩大（重写）
    @Override
    public Collection doSomething(HashMap map){//黄色标记
        System.out.println("子类被执行...");
        return map.values();
    }
}
