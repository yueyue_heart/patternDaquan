package cn.anquing.principle.liskovSubstitutionPrinciple.demo4;

import java.util.HashMap;

public class Client2 {

    public static void invoker1(){
        //父类存在的地方，子类就应该能够存在
        Father2 f = new Father2();
        HashMap map = new HashMap();
        f.doSomething(map);
    }

    public static void invoker2(){
        //父类存在的地方，子类就应该能够存在
        Son2 f =new Son2();
        HashMap map = new HashMap();
        f.doSomething(map);
    }

    public static void main(String[] args) {

        //运行结果:
        //父类被执行...
        //子类被执行...
        invoker1();
        invoker2();
    }
}
