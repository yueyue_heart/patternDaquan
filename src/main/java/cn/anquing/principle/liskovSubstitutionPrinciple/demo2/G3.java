package cn.anquing.principle.liskovSubstitutionPrinciple.demo2;

public class G3 extends Rifle {

    //狙击枪有父类没有的功能
    public void zoomOut(){
        System.out.println("通过望远镜观看敌人...");
    }

    @Override
    public void shoot(){
        System.out.println("G3射击...");
    }
}
