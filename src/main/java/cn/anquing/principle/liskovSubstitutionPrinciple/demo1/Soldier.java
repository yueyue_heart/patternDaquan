package cn.anquing.principle.liskovSubstitutionPrinciple.demo1;

public class Soldier {

    public void killEnemy(AbstractGun gun){
        System.out.println("士兵开始杀人...");
        gun.shoot();
    }
}
