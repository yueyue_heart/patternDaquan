package cn.anquing.principle.liskovSubstitutionPrinciple.demo1;

public class Client {

    public static void main(String[] args) {

        //产生三毛这个士兵
        Soldier sanMao = new Soldier();
        sanMao.killEnemy(new Rifle());
    }
}
