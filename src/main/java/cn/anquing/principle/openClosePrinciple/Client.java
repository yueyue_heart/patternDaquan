package cn.anquing.principle.openClosePrinciple;

/**
 开闭原则：是Java世界里最基础的设计原则，它指导我们如何建立一个稳定的、灵活的系统。
 定义：一个软件实体如类、模块和函数应该对扩展开放，对修改关闭。
 开闭原则的含义是说一个软件实体应该通过扩展来实现变化，而不是通过修改已有代码来实现变化。
 */
public class Client {
}
