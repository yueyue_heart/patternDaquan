package cn.anquing.pattern.singletonPattern;

public class Emperor {

    private Emperor(){
        //世俗和道德约束你，目的就是不让你产生第二个皇帝
    }

    //定义一个皇帝放在那里，然后给这个皇帝名字
/*
    private static Emperor emperor = null;

    public static Emperor getInstance(){
        if(emperor == null){ //如果皇帝还没有定义，那就定一个,这样为存在线程不安全
            emperor = new Emperor();
        }
        return emperor;
    }
*/

    //加载类时就创建一下实例
    private static final Emperor emperor = new  Emperor();

    public synchronized static Emperor getInstance(){ //此时synchronized可以不要

        return emperor;
    }





    //皇帝叫什么名字呀
    public  void emperorInfo(){
        System.out.println("我就是皇帝某某某....");
    }

}
