package cn.anquing.pattern.observerPattern.jdk.observable;

import java.util.Observable;

/**
 * Jdk中内置了Observable
 */

public class HanFeiZi2 extends Observable {


    //韩非子要吃饭了
    public void haveBreakfast(){
        System.out.println("韩非子:开始吃饭了...");
        //通知所有的观察者
        super.setChanged();
        super.notifyObservers("韩非子在吃饭");
    }

}
