package cn.anquing.pattern.observerPattern.jdk;

/*
* 观察者模式
*/


import cn.anquing.pattern.observerPattern.jdk.observable.HanFeiZi2;
import cn.anquing.pattern.observerPattern.jdk.observer.LiSi2;
import cn.anquing.pattern.observerPattern.jdk.observer.WangSi2;

import java.util.Observer;


public class Main {

    public static void main(String[] args) {


        //定义出韩非子
        HanFeiZi2 hanFeiZi = new HanFeiZi2();

        /**
         * 这部分可以优化为自动加载Observer的所有实现类
         */
        //三个观察者产生出来
        Observer liSi2 = new LiSi2();
        Observer wangSi2 = new WangSi2();
        //我们后人根据历史，描述这个场景，有三个人在观察韩非子
        hanFeiZi.addObserver(liSi2);
        hanFeiZi.addObserver(wangSi2);


        //然后这里我们看看韩非子在干什么
        hanFeiZi.haveBreakfast();

    }


}
