package cn.anquing.pattern.observerPattern.ziji.observer;

public interface Observer {

    //一发现别人有动静，自己也要行动起来
    void update(String context);
}
