package cn.anquing.pattern.observerPattern.ziji.observable;

import cn.anquing.pattern.observerPattern.ziji.observer.Observer;

public interface Observable {

    //增加一个观察者
    void addObserver(Observer observer);

    //删除一个观察者， ——我不想让你看了
    void deleteObserver(Observer observer);

    //既然要观察，我发生改变了他也应该用所动作——通知观察者
    void notifyObservers(String context);


}
