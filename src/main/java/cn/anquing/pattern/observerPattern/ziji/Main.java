package cn.anquing.pattern.observerPattern.ziji;

/*
* 观察者模式
*/


import cn.anquing.pattern.observerPattern.ziji.observable.HanFeiZi;
import cn.anquing.pattern.observerPattern.ziji.observer.Observer;
import cn.anquing.util.ClassUtil;

import java.util.List;

public class Main {

    public static void main(String[] args) {


        //定义出韩非子
        HanFeiZi hanFeiZi = new HanFeiZi();

        /**
         * 这部分可以优化为自动加载Observer的所有实现类,当添加观察者时就不需要改动代码
         */
     /*   //三个观察者产生出来
        Observer liSi = new LiSi();
        Observer wangSi = new WangSi();
        //我们后人根据历史，描述这个场景，有三个人在观察韩非子
        hanFeiZi.addObserver(liSi);
        hanFeiZi.addObserver(wangSi);*/

        //优化
        List<Class> allClassByInterface = ClassUtil.getAllClassByInterface(Observer.class);
        for(Class observerClass : allClassByInterface){
            try {
                hanFeiZi.addObserver(((Observer)observerClass.newInstance()));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }



        //然后这里我们看看韩非子在干什么
        hanFeiZi.haveBreakfast();

    }


}
