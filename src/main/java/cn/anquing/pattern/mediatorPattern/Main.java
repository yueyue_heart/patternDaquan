package cn.anquing.pattern.mediatorPattern;

/*
* 中介者模式
*/

import cn.anquing.pattern.mediatorPattern.colleague.Purchase;
import cn.anquing.pattern.mediatorPattern.colleague.Sale;
import cn.anquing.pattern.mediatorPattern.colleague.Stock;
import cn.anquing.pattern.mediatorPattern.concreteMediator.Mediator;
import cn.anquing.pattern.mediatorPattern.mediator.AbstractMediator;

/**
 * 在多个对象依赖的情况喜爱，通过加入中介者角色，取消了多个对象的关联或依关系，减少了对象的耦合性
 */

/**
 * 中介者模式的优点就是减少类间的依赖，把原有的一对多的依赖变成了一对一的依赖，同事类只依赖
 中介者，减少了依赖，当然也同时减低了类间的耦合。它的缺点呢就是中介者会膨胀的很大，而且逻辑会
 很复杂，因为所有的原本 N 个对象直接的相互依赖关系转换为中介者和同事类的依赖关系，同事类越多，
 中介者的逻辑就复杂
 */

/**
 * 使用场景：中介者模式适用于多个对象之间紧密耦合，耦合的标准可以这样来衡量：在类图中出现了蜘蛛网状结构，
 * 在这种情况下一定要考虑使用中介者 模式，有利于把蜘蛛网梳理为一个星型结构，使原本复杂混乱关系变得清晰简单
 * 大家可以在如下的情况下尝试使用中介者模式：
 1、 N 个对象之间产生了相互的依赖关系，其中 N 大于 2，注意是相互的依赖；
 2、 多个对象有依赖关系，但是依赖的行为尚不确定或者有发生改变的可能，在这种情况下一般建议采用中介者模式，降低变更引起的风险扩散；
 3、 产品开发。其中一个明显的例子就是 MVC 框架，把这个应用到产品中，可以提升产品的性能和扩展性，但是作为项目开发就未必，项目是以交付投产为目标，而产品以稳定、高效、扩展为宗旨。
 */

/**
 * 中介者模式很少用到接口或者抽象类:
 * 首先说同事类，既然是同事类而不是兄弟类（有相同的血缘）那也说明这些类之间是协作关系，
 * 完成不同的任务，处理不同的业务，所以不能在抽象类或接口中严格定义同事类必须具有的方法（从这点也可以看出继承是侵入性的），这是不合适的。
 * 其次一个中介者抽象类一般只有一个实现者，除非中介者逻辑非常大，代码量非常高，这时才会出现多个中介者的情况，所有，对中介者来说，抽象已经没有太多的必要
 */

public class Main {

    public static void main(String[] args) {

        //初始化中介者
        AbstractMediator mediator = new Mediator();

        Purchase purchase = new Purchase(mediator);
        Sale sale = new Sale(mediator);
        Stock stock = new Stock(mediator);

        mediator.setPurchase(purchase);
        mediator.setSale(sale);
        mediator.setStock(stock);


        //采购人员采购电脑
        System.out.println("------采购人员采购电脑--------");
        purchase.buyIBMcomputer(100);

        //销售人员销售电脑
        System.out.println("\n------销售人员销售电脑--------");
        sale.sellIBMComputer(1);

        //库房管理人员管理库存
        System.out.println("\n------库房管理人员清库处理--------");
        stock.clearStock();
    }


}
