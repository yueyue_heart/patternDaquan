package cn.anquing.pattern.mediatorPattern.mediator;

import cn.anquing.pattern.mediatorPattern.colleague.Purchase;
import cn.anquing.pattern.mediatorPattern.colleague.Sale;
import cn.anquing.pattern.mediatorPattern.colleague.Stock;

public abstract class AbstractMediator {

    protected Purchase purchase;

    protected Sale sale;

    protected Stock stock;

/*    //构造函数
    public AbstractMediator(){
        purchase = new Purchase(this);
        sale = new Sale(this);
        stock = new Stock(this);
    }*/

    //通过getter/setter方法把同事类注入进来,不使用构造方法的原因是对于中介者来说，并不是所有的同事类都会用到
    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }




    //中介者最重要的方法，叫做事件方法，处理多个对象之间的关系
    public abstract void execute(String str,Object...objects);
}
