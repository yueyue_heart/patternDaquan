package cn.anquing.pattern.mediatorPattern.colleague;

import cn.anquing.pattern.mediatorPattern.mediator.AbstractMediator;

public class AbstractColleague {

    protected AbstractMediator mediator;

    //此处一定要使用有参构造，因为对于同事类，中介者是必须的
    public AbstractColleague(AbstractMediator _mediator){
        this.mediator = _mediator;
    }
}
