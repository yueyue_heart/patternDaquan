package cn.anquing.pattern.buildPattern2.director;

import cn.anquing.pattern.buildPattern2.builder.Builder;

public class Director {

    //指挥装机人员组装电脑
    public void Construct(Builder builder){

        builder.BuildCPU();
        builder.BuildMainboard();
        builder.BuildHD();
    }

}
