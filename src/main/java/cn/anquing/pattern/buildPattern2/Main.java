package cn.anquing.pattern.buildPattern2;

import cn.anquing.pattern.buildPattern2.builder.Builder;
import cn.anquing.pattern.buildPattern2.builder.ConcreteBuilder;
import cn.anquing.pattern.buildPattern2.director.Director;
import cn.anquing.pattern.buildPattern2.model.Computer;

/**
 * 实例概况

 背景：小成希望去电脑城买一台组装的台式主机
 过程：
 电脑城老板（Diretor）和小成（Client）进行需求沟通（买来打游戏？学习？看片？）
 了解需求后，电脑城老板将小成需要的主机划分为各个部件（Builder）的建造请求（CPU、主板blabla）
 指挥装机人员（ConcreteBuilder）去构建组件；
 将组件组装起来成小成需要的电脑（Product）

 作者：Carson_Ho
 链接：https://www.jianshu.com/p/be290ccea05a
 來源：简书
 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Main {

    public static void main(String[] args) {

        //逛了很久终于发现一家合适的电脑店
        //找到该店的老板和装机人员
        Director director = new Director();
        Builder builder = new ConcreteBuilder();

        //沟通需求后，老板叫装机人员去装电脑
        director.Construct(builder);

        //装完后，组装人员搬来组装好的电脑
        Computer computer = builder.GetComputer();

        //组装人员展示电脑给小成看
        computer.Show();

    }



}
