package cn.anquing.pattern.interpreterPattern;

/*
* 解释器模式,太难了
*/

import cn.anquing.pattern.interpreterPattern.calculator.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 *解释器模式在实际的系统开发中使用得非常少， 因为它会引起效率、 性能以及维护等问
 题， 一般在大中型的框架型项目能够找到它的身影， 如一些数据分析工具、 报表设计工具、
 科学计算工具等， 若你确实遇到“一种特定类型的问题发生的频率足够高”的情况， 准备使用
 解释器模式时， 可以考虑一下Expression4J、 MESP（Math Expression String Parser） 、 Jep等
 开源的解析工具包（这三个开源产品都可以通过百度、 Google搜索到， 请读者自行查询） ，
 功能都异常强大， 而且非常容易使用， 效率也还不错， 实现大多数的数学运算完全没有问
 题， 自己没有必要从头开始编写解释器。 有人已经建立了一条康庄大道， 何必再走自己的泥
 泞小路呢？
 */


public class Main {


        //运行四则运算
        public static void main(String[] args) throws IOException {

            String expStr = getExpStr();
            //赋值
            HashMap<String,Integer> var = getValue(expStr);

            Calculator cal = new Calculator(expStr);

            System.out.println("运算结果为： "+expStr +"="+cal.run(var));
        }


        //获得表达式
        public static String getExpStr() throws IOException{

            System.out.print("请输入表达式： ");
            return (new BufferedReader(new InputStreamReader(System.in))).readLine();
        }

        //获得值映射
        public static HashMap<String,Integer> getValue(String exprStr) throws IOException{

            HashMap<String,Integer> map = new HashMap<String,Integer>();
            //解析有几个参数要传递
            for(char ch:exprStr.toCharArray()){
                if(ch != '+' && ch != '-'){
                    //解决重复参数的问题
                    if(!map.containsKey(String.valueOf(ch))){
                        String in = (new BufferedReader(new InputStreamReader(System.in))).readLine();
                        map.put(String.valueOf(ch),Integer.valueOf(in));
                    }
                }
            }
            return map;
        }

}


