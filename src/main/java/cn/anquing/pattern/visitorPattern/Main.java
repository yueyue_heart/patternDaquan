package cn.anquing.pattern.visitorPattern;

/*
* 访问者模式
*/

import cn.anquing.pattern.visitorPattern.employee.CommonEmployee;
import cn.anquing.pattern.visitorPattern.employee.Employee;
import cn.anquing.pattern.visitorPattern.employee.Manager;
import cn.anquing.pattern.visitorPattern.visitor.IShowVisitor;
import cn.anquing.pattern.visitorPattern.visitor.ITotalVisitor;
import cn.anquing.pattern.visitorPattern.visitor.ShowVisitor;
import cn.anquing.pattern.visitorPattern.visitor.TotalVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 *统计功能：除了这种海量数据外，我建议数据统计和报表的批处理通过访问者模式来处理会比较简单(此处举例：统计工资)
 *多个访问者:showVisitor想要报表，totalVisitor想要工资汇总
 *拦截器：拦截器的核心作用对被拦截的对象进行检查和修改数据，具备了这两个功能，拦截器的雏形就有了，访问者模式就可以实现简单的拦截器角色
 */

public class Main {

    public static void main(String[] args) {

        //展示报表访问者
        IShowVisitor showVisitor = new ShowVisitor();
        //汇总报表的访问者
        ITotalVisitor totalVisitor = new TotalVisitor();

        for(Employee emp:mockEmployee()){

            emp.accept(showVisitor);//接受展示报表访问者
            emp.accept(totalVisitor);//接受汇总表访问者
        }

        //展示报表
        showVisitor.report();

        //汇总报表
        totalVisitor.totalSalary();

    }
    //模拟出公司的人员情况，我们可以想象这个数据室通过持久层传递过来的
    public static List<Employee> mockEmployee() {
        List<Employee> empList = new ArrayList<Employee>();

        //产生张三这个员工
        CommonEmployee zhangSan = new CommonEmployee();
        zhangSan.setJob("编写Java程序，绝对的蓝领、苦工加搬运工");
        zhangSan.setName("张三");
        zhangSan.setSalary(1800);
        zhangSan.setSex(Employee.MALE);
        empList.add(zhangSan);

        //产生李四这个员工
        CommonEmployee liSi = new CommonEmployee();
        liSi.setJob("页面美工，审美素质太不流行了！ ");
        liSi.setName("李四");
        liSi.setSalary(1900);
        liSi.setSex(Employee.FEMALE);
        empList.add(liSi);

        //再产生一个经理
        Manager wangWu = new Manager();
        wangWu.setName("王五");
        wangWu.setPerformance("基本上是负值，但是我会拍马屁呀");
        wangWu.setSalary(18750);
        wangWu.setSex(Employee.MALE);
        empList.add(wangWu);
        return empList;
    }


}
