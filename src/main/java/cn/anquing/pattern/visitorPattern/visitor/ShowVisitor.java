package cn.anquing.pattern.visitorPattern.visitor;

import cn.anquing.pattern.visitorPattern.employee.CommonEmployee;
import cn.anquing.pattern.visitorPattern.employee.Employee;
import cn.anquing.pattern.visitorPattern.employee.Manager;

public class ShowVisitor implements IShowVisitor {

    private String info = "";


    @Override
    public void report() {
        System.out.println(this.info);
    }

    @Override
    public void visit(CommonEmployee commonEmployee) {
        info = info + this.getCommonEmployee(commonEmployee)+"\n";
    }

    @Override
    public void visit(Manager manager) {
        info = info + this.getManagerInfo(manager)+"\n";

    }


    //组装出基本信息
    private String getBasicInfo(Employee employee){
        String info = "姓名： " + employee.getName() + "\t";
        info = info + "性别： " + (employee.getSex() == Employee.FEMALE?"女":"男")
                + "\t";
        info = info + "薪水： " + employee.getSalary() + "\t";
        return info;
    }

    //组装出部门经理的信息
    private String getManagerInfo(Manager manager){
        String basicInfo = this.getBasicInfo(manager);
        String otherInfo = "业绩： "+manager.getPerformance() + "\t";
        return basicInfo + otherInfo;
    }

    //组装出普通员工信息
    private String getCommonEmployee(CommonEmployee commonEmployee){
        String basicInfo = this.getBasicInfo(commonEmployee);
        String otherInfo = "工作： "+commonEmployee.getJob()+"\t";
        return basicInfo + otherInfo;
    }
}
