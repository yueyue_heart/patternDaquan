package cn.anquing.pattern.visitorPattern.visitor;


import cn.anquing.pattern.visitorPattern.employee.CommonEmployee;
import cn.anquing.pattern.visitorPattern.employee.Manager;

/**
 *
 */
public interface IVisitor {

    //首先定义我可以访问普通员工
    void visit(CommonEmployee commonEmployee);

    //其次定义，我还可以访问部门经理
    void visit(Manager manager);

}
