package cn.anquing.pattern.visitorPattern.visitor;

import cn.anquing.pattern.visitorPattern.employee.CommonEmployee;
import cn.anquing.pattern.visitorPattern.employee.Manager;

public class TotalVisitor implements ITotalVisitor {


    //部门经理的工资系数是5
    private final static int MANAGER_COEFFICIENT = 5;

    //员工的工资系数是2
    private final static int COMMONEMPLOYEE_COEFFICIENT = 2;

    //普通员工的工资总和
    private int commonTotalSalary = 0;

    //部门经理的工资总和
    private int managerTotalSalary =0;


    @Override
    public void totalSalary() {
        System.out.println("本公司的月工资总额是" + (this.commonTotalSalary + this.managerTotalSalary));

    }


    @Override
    public void visit(CommonEmployee commonEmployee) {
        //计算普通员工的薪水总和
        this.calCommonSlary(commonEmployee.getSalary());
    }

    @Override
    public void visit(Manager manager) {
        //计算部门经理的工资总和
        this.calManagerSalary(manager.getSalary());
    }



    //计算部门经理的工资总和
    private void calManagerSalary(int salary){
        this.managerTotalSalary = this.managerTotalSalary + salary
                *MANAGER_COEFFICIENT ;
    }
    //计算普通员工的工资总和
    private void calCommonSlary(int salary){
        this.commonTotalSalary = this.commonTotalSalary +
                salary*COMMONEMPLOYEE_COEFFICIENT;
    }
}
