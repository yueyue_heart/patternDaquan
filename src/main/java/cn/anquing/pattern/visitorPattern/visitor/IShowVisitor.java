package cn.anquing.pattern.visitorPattern.visitor;

public interface IShowVisitor extends IVisitor {

    //展示报表
    public void report();
}
