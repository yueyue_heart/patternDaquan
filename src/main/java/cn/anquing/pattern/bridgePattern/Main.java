package cn.anquing.pattern.bridgePattern;



/*
*  桥梁模式
*  业务抽象角色引用业务实现角色，或者说业务抽象角色的部分实现是由业务实现角色完成的
*  桥梁模式的优点就是类间解耦，我们上面已经提到，两个角色都可以自己的扩展下去，不会相互影响，这个也符合 OCP 原则
*/

import cn.anquing.pattern.bridgePattern.abstraction.HouseCrop;
import cn.anquing.pattern.bridgePattern.abstraction.ShanzhaiCrop;
import cn.anquing.pattern.bridgePattern.implementor.Clothes;
import cn.anquing.pattern.bridgePattern.implementor.House;
import cn.anquing.pattern.bridgePattern.implementor.Ipod;

/**
 * 解决了继承的关联性缺点
 */
public class Main {

    public static void main(String[] args) {


        HouseCrop houseCrop = new HouseCrop(new House());
        houseCrop.makeMoney();

        ShanzhaiCrop shanzhaiCrop = new ShanzhaiCrop(new Ipod());
        shanzhaiCrop.makeMoney();

        ShanzhaiCrop shanzhaiCrop1 = new ShanzhaiCrop(new Clothes());
        shanzhaiCrop1.makeMoney();

    }



}
