package cn.anquing.pattern.bridgePattern.abstraction;

import cn.anquing.pattern.bridgePattern.implementor.House;

public class HouseCrop extends Crop {


    public HouseCrop(House house) {
        super(house);
    }

    @Override
    public void makeMoney() {
        super.makeMoney();
        System.out.println("===========房地产赚大钱了===============");
    }
}
