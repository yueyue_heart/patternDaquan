package cn.anquing.pattern.bridgePattern.abstraction;

import cn.anquing.pattern.bridgePattern.implementor.Product;

public class ShanzhaiCrop extends Crop {


    public ShanzhaiCrop(Product product) {
        super(product);
    }

    @Override
    public void makeMoney() {
        super.makeMoney();
        System.out.println("===========做山寨赚大钱了===============");
    }
}


