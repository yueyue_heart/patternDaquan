package cn.anquing.pattern.bridgePattern.abstraction;

import cn.anquing.pattern.bridgePattern.implementor.Product;

public abstract class Crop {

    private Product product;

    public Crop(Product product){
        this.product = product;
    }

    public void makeMoney(){
       product.beProducted();
       product.beSelled();
    }
}
