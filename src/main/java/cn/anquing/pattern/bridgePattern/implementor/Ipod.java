package cn.anquing.pattern.bridgePattern.implementor;

public class Ipod extends Product {


    @Override
    public void beProducted() {
        System.out.println("============生产山寨ipod=============");
    }

    @Override
    public void beSelled() {
        System.out.println("============卖山寨ipod=============");
    }
}
