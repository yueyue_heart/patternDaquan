package cn.anquing.pattern.bridgePattern.implementor;

public class Clothes extends Product {


    @Override
    public void beProducted() {

        System.out.println("============生产山寨衣服=============");

    }

    @Override
    public void beSelled() {
        System.out.println("============卖山寨衣服=============");
    }
}
