package cn.anquing.pattern.bridgePattern.implementor;

public class House extends Product {
    @Override
    public void beProducted() {
        System.out.println("============建房子=============");
    }

    @Override
    public void beSelled() {
        System.out.println("============卖房子=============");
    }
}
