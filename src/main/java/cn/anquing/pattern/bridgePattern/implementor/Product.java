package cn.anquing.pattern.bridgePattern.implementor;

public abstract class Product {

    public abstract void beProducted();

    public abstract void beSelled();
}
