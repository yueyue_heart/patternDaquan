package cn.anquing.pattern.templateMethodPattern;

import cn.anquing.pattern.templateMethodPattern.template.HummerModel;

public class HummerH1Model  extends HummerModel {

    @Override
    public void alarm() {
        System.out.println("悍马H1鸣笛...");
    }

    @Override
    public void engineBoom() {
        System.out.println("悍马H1引擎声音是这样在...");
    }

    @Override
    public void start() {
        System.out.println("悍马H1发动...");
    }

    @Override
    public void stop() {
        System.out.println("悍马H1停车...");
    }
}
