package cn.anquing.pattern.templateMethodPattern;

public class Client {

    public static void main(String[] args) {
        HummerH2Model h2 = new HummerH2Model();
        h2.run(); //H2型号的悍马跑起来

        HummerH1Model h1 = new HummerH1Model();
        h1.run(); //H1型号的悍马跑起来
    }
}
