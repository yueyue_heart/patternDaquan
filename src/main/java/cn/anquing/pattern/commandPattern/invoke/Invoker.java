package cn.anquing.pattern.commandPattern.invoke;

import cn.anquing.pattern.commandPattern.comman.Command;

/**
 * Invoker 角色：调用者，接收到命令，并执行命令，例子中我这里项目经理就是这个角色
 */
public class Invoker {

    private Command command;

    public void setCommand(Command _command) {
        this.command = _command;
    }

   public void action(){
       command.execute();
   }


}
