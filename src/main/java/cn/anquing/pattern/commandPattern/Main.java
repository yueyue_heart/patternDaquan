package cn.anquing.pattern.commandPattern;

import cn.anquing.pattern.commandPattern.comman.AddRequirementCommand;
import cn.anquing.pattern.commandPattern.comman.DeletePageCommand;
import cn.anquing.pattern.commandPattern.invoke.Invoker;

/*
* 命令模式
*/

/**
 * 组织间不用直接找对实现功能的人，找他们的头就可以了
 */

public class Main {

    public static void main(String[] args) {

        Invoker invoker = new Invoker();
        invoker.setCommand(new DeletePageCommand());
        invoker.action();


        invoker.setCommand(new AddRequirementCommand());
        invoker.action();

    }



}
