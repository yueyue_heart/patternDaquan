package cn.anquing.pattern.commandPattern.comman;

import cn.anquing.pattern.commandPattern.receiver.CodeGroup;
import cn.anquing.pattern.commandPattern.receiver.PageGroup;
import cn.anquing.pattern.commandPattern.receiver.RequirementGroup;

/**
 * Command 角色：就是命令，需要我执行的所有命令都这里声明
 */
public abstract class Command {

    //把三个组都定义好，子类可以直接使用
    protected RequirementGroup rg = new RequirementGroup(); //需求组您的设计模式

    protected PageGroup pg = new PageGroup(); //美工组

    protected CodeGroup cg = new CodeGroup(); //代码组



    //只要一个方法，你要我做什么事情
    public abstract void execute();


}
