package cn.anquing.pattern.factory.abstractFactoryModel;

public interface PersonFactory {

	Boy getBoy();
	
	Girl getGirl();
}
