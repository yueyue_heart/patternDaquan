package cn.anquing.pattern.factory.abstractFactoryModel;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PersonFactory  mf = new MCFactory();
		PersonFactory  hf = new HNFactory();
		
		mf.getBoy().drawMan();
		mf.getGirl().drawWomen();
		
		hf.getBoy().drawMan();
		hf.getGirl().drawWomen();
	}

}
