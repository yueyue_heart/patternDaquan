package cn.anquing.pattern.factory.factoryMethodModel;

import java.util.Map;

public class HairFactory {

	/**
	 *根据key值获取对象
	 * @param key
	 * @return
	 */
	public HairInterface getHair(String key){
		if("left".equals(key)){
			HairInterface hairInterface = new LeftHair();
			return hairInterface;
		}else if("right".equals(key)){
			HairInterface hairInterface = new RightHair();
			return hairInterface;
		}else{
			return null;
		}

	}

	/**
	 *根据key值获取对象
	 * @param clazz
	 * @return
	 */
	public HairInterface getHairByClass(Class clazz){
		HairInterface hairInterface = null;
		try {
			hairInterface = (HairInterface)Class.forName(clazz.getName()).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hairInterface;

	}
	/**
	 * 根据类名获取对象
	 * @param className
	 * @return
	 */
	public HairInterface getHairByClassName(String className){
		HairInterface hairInterface = null;
		try {
			hairInterface = (HairInterface)Class.forName(className).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hairInterface;
	}


	/**
	 * 根据类名关键字获取对象（type.properties）
	 * @param key
	 * @return
	 */
	public HairInterface getHairByClassKey(String key){
		PropertiesReader propertiesReader = new PropertiesReader();
		Map<String,String> map = propertiesReader. getProperties();
		String className = map.get(key);
		HairInterface hairInterface = null;
		try {
			hairInterface = (HairInterface)Class.forName(className).newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hairInterface;
	}
}
