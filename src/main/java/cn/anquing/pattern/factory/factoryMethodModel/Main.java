package cn.anquing.pattern.factory.factoryMethodModel;

public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {

//		获取实例的普通方式
//		HairInterface leftHair = new LeftHair();
//		leftHair.draw();
//		HairInterface rightHair = new RightHair();
//		rightHair.draw();

//		使用工厂方法
		HairFactory hairFactory = new HairFactory();
//		根据key值获取对象
		System.out.println("根据key值获取对象");
		HairInterface leftHair = hairFactory.getHair("left");
		leftHair.draw();
		HairInterface rightHair = hairFactory.getHair("right");
		rightHair.draw();


//		根据类名获取对象
		System.out.println("根据类获取对象");
		HairInterface leftHairByClass = hairFactory.getHairByClass(LeftHair.class);
		leftHairByClass.draw();
		HairInterface rightHairByClass = hairFactory.getHairByClass(RightHair.class);
		rightHairByClass.draw();


//		根据类名获取对象
		System.out.println("根据类名获取对象");
		HairInterface leftHairByClassName = hairFactory.getHairByClassName("LeftHair");
		leftHairByClassName.draw();
		HairInterface rightHairByClassName = hairFactory.getHairByClassName("RightHair");
		rightHairByClassName.draw();

//		根据类名关键字获取对象（type.properties）
		System.out.println("根据类名关键字获取对象（type.properties）");
		HairInterface leftHairByClassKey = hairFactory.getHairByClassKey("left");
		leftHairByClassKey.draw();
		HairInterface rightHairByClassKey = hairFactory.getHairByClassKey("right");
		rightHairByClassKey.draw();
		HairInterface inHairByClassKey = hairFactory.getHairByClassKey("in");
		inHairByClassKey.draw();


	}


}
