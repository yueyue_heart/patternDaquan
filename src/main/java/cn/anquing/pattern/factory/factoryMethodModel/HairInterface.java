package cn.anquing.pattern.factory.factoryMethodModel;

public interface HairInterface {

	void draw();
}
