package cn.anquing.pattern.factory.factoryMethodModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesReader {
	
	public Map<String,String> getProperties(){
		Properties props = new Properties();
		Map<String,String> map = new HashMap<String,String>();
		
		try {
			URL resource = getClass().getResource("/");
			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("type.properties");
//			InputStream in = getClass().getResourceAsStream("");
			props.load(in);
			Enumeration en = props.propertyNames();
			while (en.hasMoreElements()){
				String key = (String)en.nextElement();
				String property = props.getProperty(key);
				map.put(key,property);
				//System.out.println(key+"|"+property);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	public static void main(String[] args) {
		PropertiesReader propertiesReader = new PropertiesReader();
		Map<String,String> map = propertiesReader. getProperties();		
	}
}
