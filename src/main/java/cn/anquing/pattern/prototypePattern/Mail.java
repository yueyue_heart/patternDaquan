package cn.anquing.pattern.prototypePattern;

/**
*Java 提供了一个 Cloneable 接口来标示这个对象是可拷贝的，为什么说是“标示”呢？翻开 JDK 的帮助看看 Cloneable 是一个方法都没有的，这个接口只是一个标记作用
 */

public class Mail implements Cloneable {

    /**
     * 重写clone()，在 Java 中所有类的老祖宗是谁？对嘛，Object 类，每个类默认都是继承了这个类，所以这个用上重写是非常正确的。
     * 对象拷贝时，类的构造函数是不会被执行的:
     * Object 类的clone 方法的原理是从内存中（具体的说就是堆内存）以二进制流的方式进行拷贝，重新分配一个内存块，那构造函数没有被执行也是非常正常的了
     * 浅拷贝和深拷贝问题
     *
     */

    //收件人
    private String receiver;
    //邮件名称
    private String subject;
    //称谓
    private String appellation;
    //邮件内容
    private String contxt;
    //邮件的尾部，一般都是加上“XXX版权所有”等信息
    private String tail;
    //构造函数
    public Mail(AdvTemplate advTemplate){
        this.contxt = advTemplate.getAdvContext();
        this.subject = advTemplate.getAdvSubject();
    }

    @Override
    public Mail clone(){
        Mail mail =null;
        try {
            mail = (Mail)super.clone();//对象拷贝时，类的构造函数是不会被执行的。
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return mail;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getContxt() {
        return contxt;
    }

    public void setContxt(String contxt) {
        this.contxt = contxt;
    }

    public String getTail() {
        return tail;
    }

    public void setTail(String tail) {
        this.tail = tail;
    }
}
