package cn.anquing.pattern.prototypePattern.clone.deep;


/**
 * *深拷贝
 * Clone 与 final 两对冤家。对象的 clone 与对象内的 final 属性是由冲突的
 */
public class Client {

    public static void main(String[] args) {

        //产生一个对象
        Deep deep = new Deep();
        //设置一个值
        deep.setValue("张三");
        //拷贝一个对象
        Deep cloneDeep = deep.clone();
        cloneDeep.setValue("李四");//Thing中的私有对象arrayList也发生的变化

        System.out.println(deep.getValue());

        //运行结果：[张三]，如果是浅clone，运行结果应该是[张三,李四]
    }
}
