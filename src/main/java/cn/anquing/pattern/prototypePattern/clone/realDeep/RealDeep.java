package cn.anquing.pattern.prototypePattern.clone.realDeep;

import java.io.Serializable;
import java.util.ArrayList;

public class RealDeep implements Serializable{

    //定义一个私有变量
    private ArrayList<String> arrayList = new ArrayList<String>();//如果添加final，以下就要报错：thing.arrayList = (ArrayList<String>)this.arrayList.clone();

    //设置HashMap的值
    public void setValue(String value){
        this.arrayList.add(value);
    }

    //取得arrayList的值
    public ArrayList<String> getValue(){
        return this.arrayList;
    }
}
