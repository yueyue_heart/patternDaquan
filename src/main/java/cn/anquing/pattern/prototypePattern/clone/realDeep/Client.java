package cn.anquing.pattern.prototypePattern.clone.realDeep;


import cn.anquing.util.CloneUtil;

/**
 * 通过对对象的序列化和反序列化实现克隆，可达到的真正深度克隆(使用越来更简单)
 */
public class Client {

    public static void main(String[] args) throws Exception{

        //产生一个对象
        RealDeep realDeep = new RealDeep();
        //设置一个值
        realDeep.setValue("张三");
        //拷贝一个对象
        RealDeep realDeepOfClone = CloneUtil.clone(realDeep);
        realDeepOfClone.setValue("李四");//Thing中的私有对象arrayList也发生的变化

        System.out.println(realDeep.getValue());

        //运行结果：[张三]，如果是浅clone，运行结果应该是[张三,李四]
    }
}
