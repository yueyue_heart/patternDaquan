package cn.anquing.pattern.prototypePattern.clone.deep;

import java.util.ArrayList;

public class Deep implements Cloneable{

    //定义一个私有变量
    private ArrayList<String> arrayList = new ArrayList<String>();//如果添加final，以下就要报错：thing.arrayList = (ArrayList<String>)this.arrayList.clone();

    @Override
    public Deep clone(){
        Deep deep =null;
        try {
            deep = (Deep)super.clone();
            deep.arrayList = (ArrayList<String>)this.arrayList.clone();//对内部的数组和引用对象也进行clone一下，实现深拷贝
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return deep;
    }

    //设置HashMap的值
    public void setValue(String value){
        this.arrayList.add(value);
    }

    //取得arrayList的值
    public ArrayList<String> getValue(){
        return this.arrayList;
    }
}
