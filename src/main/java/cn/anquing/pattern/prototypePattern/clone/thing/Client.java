package cn.anquing.pattern.prototypePattern.clone.thing;


/**
 * *浅拷贝（一个种非常不安全的方式）: Java做了一个偷懒的拷贝动作Object类提供的方法 clone只是拷贝本对象，其对象内部的数组、引用对象等都不拷贝，还是指向原生对象的内部元素地址
 * 为什么在 Mail 那个类中就可以使用使用 String 类型，而不会产生由浅拷贝带来的问题呢？内部的数组和引用对象才不拷贝，其他的原始类型比如int,long,String(Java 就希望你把 String 认为是基本类型，String 是没有 clone 方法的)等都会被拷贝的
 *
 */
public class Client {

    public static void main(String[] args) {

        //产生一个对象
        Thing thing = new Thing();
        //设置一个值
        thing.setValue("张三");
        //拷贝一个对象
        Thing cloneThing = thing.clone();
        cloneThing.setValue("李四");//Thing中的私有对象arrayList也发生的变化

        System.out.println(thing.getValue());

        //运行结果：[张三, 李四]，如果是深clone，运行结果应该是[张三]
    }
}
