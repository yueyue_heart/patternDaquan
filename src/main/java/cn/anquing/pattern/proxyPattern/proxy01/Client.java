package cn.anquing.pattern.proxyPattern.proxy01;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ExtendsCar extendsCar = new ExtendsCar();
		extendsCar.move();
	}

}
