package cn.anquing.pattern.proxyPattern.proxy01;

public interface Movable {

	public void move();
}
