package cn.anquing.pattern.proxyPattern.proxy01;

public class ExtendsCar extends Car {
	
	@Override
	public void move() {
		long startTime = System.currentTimeMillis();
		System.out.println("汽车开始行驶...");
		super.move();
		long endTime = System.currentTimeMillis();
		System.out.println("汽车结束行驶... 总耗时"+(endTime-startTime));
	}

}
