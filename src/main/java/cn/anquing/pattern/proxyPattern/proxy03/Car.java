package cn.anquing.pattern.proxyPattern.proxy03;

import java.util.Random;

/**
 * @author Administrator
 *Movable实现类
 */
public class Car implements Movable {

	@Override
	public void move() {
		try {
			Thread.sleep(new Random().nextInt(1000));
			System.out.println("汽车行驶中...");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
