package cn.anquing.pattern.proxyPattern.proxy03;



public class Client {
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Car car = new Car();
		Movable carTime = new CarTimeProxy(car);
		carTime.move();
	}

}
