package cn.anquing.pattern.proxyPattern.proxy03;

public interface Movable {

	public void move();
}
