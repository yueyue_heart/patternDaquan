package cn.anquing.pattern.proxyPattern.proxy03;

public class CarTimeProxy implements Movable {

    public CarTimeProxy(Movable m) {
        super();
        this.m = m;
    }
    private Movable m;
    @Override
    public void move() {
        long startTime = System.currentTimeMillis();
        System.out.println("汽车开始行驶...");
        m.move();
        long endTime = System.currentTimeMillis();
        System.out.println("汽车结束行驶... 总耗时"+(endTime-startTime));
    }
}
