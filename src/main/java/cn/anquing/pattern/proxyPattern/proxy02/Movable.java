package cn.anquing.pattern.proxyPattern.proxy02;

public interface Movable {

	public void move();
}
