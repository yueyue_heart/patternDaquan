package cn.anquing.pattern.proxyPattern.proxy02;

public class Car1 implements Movable {

	Movable car;
	public Car1(Movable car) {		
		this.car = car;
	}
	@Override
	public void move() {
		long startTime = System.currentTimeMillis();
		System.out.println("汽车开始行驶...");
		car.move();
		long endTime = System.currentTimeMillis();
		System.out.println("汽车结束行驶... 总耗时"+(endTime-startTime));
	}

}
