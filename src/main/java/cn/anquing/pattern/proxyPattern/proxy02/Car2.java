package cn.anquing.pattern.proxyPattern.proxy02;

public class Car2 implements Movable {

	Movable car;
	public Car2(Movable car) {
		this.car = car;
	}
	@Override
	public void move() {
		System.out.println("汽车行驶日志start...");
		car.move();
		System.out.println("汽车行驶日志end...");
	}

}
