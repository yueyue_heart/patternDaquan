package cn.anquing.pattern.proxyPattern.proxy02;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
/*		Car car = new Car();
		Car1 car1 = new Car1(car);
		car1.move();*/

/*		Car car = new Car();
		Car1 car1 = new Car1(car);
		Car2 car2 = new Car2(car1);
		car2.move();*/

		Car car = new Car();
		Car2 car2 = new Car2(car);
		Car1 car1 = new Car1(car2);
		car1.move();
	}

}
