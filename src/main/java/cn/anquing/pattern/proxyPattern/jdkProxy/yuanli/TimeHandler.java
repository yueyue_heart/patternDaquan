package cn.anquing.pattern.proxyPattern.jdkProxy.yuanli;

import java.lang.reflect.Method;

public class TimeHandler implements InvocationHandler {

    private Object target;

    public TimeHandler(Object target) {
        this.target = target;
    }

    @Override
    public void invoke(Object proxy, Method method) {
        try {
            long startTime = System.currentTimeMillis();
            System.out.println("汽车开始行驶...");
            method.invoke(target);
            long endTime = System.currentTimeMillis();
            System.out.println("汽车结束行驶... 总耗时"+(endTime-startTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
