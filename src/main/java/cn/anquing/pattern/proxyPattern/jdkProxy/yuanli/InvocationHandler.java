package cn.anquing.pattern.proxyPattern.jdkProxy.yuanli;

import java.lang.reflect.Method;

public interface InvocationHandler {

    void invoke(Object proxy, Method method);
}
