package cn.anquing.pattern.proxyPattern.jdkProxy.yuanli;

public class Client {

    public static void main(String[] args) throws Exception {

        Car car = new Car();
        TimeHandler th = new TimeHandler(car);
        Movable movable = (Movable)Proxy.newProxyInstance(Movable.class,th);
        movable.move();
    }
}
