package cn.anquing.pattern.proxyPattern.jdkProxy.demo;

import java.util.Random;

/**
 * @author Administrator
 *Movable实现类
 */
public class Train implements Movable {

	@Override
	public void move() {
		try {
			Thread.sleep(new Random().nextInt(1000));
			System.out.println("火车行驶中...");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void fly() {
		try {
			Thread.sleep(new Random().nextInt(1000));
			System.out.println("火车飞行中...");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
