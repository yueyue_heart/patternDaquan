package cn.anquing.pattern.proxyPattern.jdkProxy.demo;

public interface Movable {

	void move();

	void fly();
}
