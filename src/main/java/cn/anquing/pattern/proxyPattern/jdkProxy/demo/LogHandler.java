package cn.anquing.pattern.proxyPattern.jdkProxy.demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class LogHandler implements InvocationHandler {

    Object target; //目标对象

    public LogHandler(Object target) {
        this.target = target;
    }

    /**
     *
     * @param proxy 代理对象
     * @param method 目标对象的方法
     * @param args 方法参数
     * @return 方法的返回值
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("行驶日志start...");
        method.invoke(target);
        System.out.println("行驶日志end...");
        return null;
    }
}
