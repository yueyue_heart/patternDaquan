package cn.anquing.pattern.proxyPattern.jdkProxy.demo;

import java.lang.reflect.Proxy;

/*
* 通过：System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
* 可以将动态生成的代理类写入工程中，便于查看验证
* */

/**
 * JDK动态代理
 * 1、只能代理实现了接口的类
 * 2、没有实现接口的类不能实现JOK的动态代理
 */


public class Client {

    public void carDriver(){
        //行驶时间
/*        Car car = new Car();
        Class<?> aClass = car.getClass();
        TimeHandler timeHandler = new TimeHandler(car);
        Movable timeCar = (Movable)Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(), timeHandler);
        timeCar.move();*/

        //先记录日志，再记录时间
        Car car = new Car();
        Class<?> aClass = car.getClass();

        TimeHandler timeHandler = new TimeHandler(car);
        Movable timeCar = (Movable)Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(), timeHandler);

        LogHandler logHandler = new LogHandler(timeCar);
        Movable logCar = (Movable)Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(), logHandler);

//        logCar.move();
        logCar.fly();

        //先记录时间，再记录日志
/*        Car car = new Car();
        Class<?> aClass = car.getClass();

        LogHandler logHandler = new LogHandler(car);
        Movable logCar = (Movable)Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(), logHandler);

        TimeHandler timeHandler = new TimeHandler(logCar);
        Movable timeCar = (Movable)Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(), timeHandler);

        timeCar.move()*/;
    }

    public void trainDriver(){
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        //行驶时间
        /*Train train = new Train();
        Class<?> tClass = train.getClass();
        TimeHandler timeHandler = new TimeHandler(train);
        Movable timeTrain = (Movable)Proxy.newProxyInstance(tClass.getClassLoader(), tClass.getInterfaces(), timeHandler);
        timeTrain.move();*/

        //先记录日志，再记录时间
/*        Train train = new Train();
        Class<?> tClass = train.getClass();
        TimeHandler timeHandler = new TimeHandler(train);
        Movable timeTrain = (Movable)Proxy.newProxyInstance(tClass.getClassLoader(), tClass.getInterfaces(), timeHandler);

        LogHandler logHandler = new LogHandler(timeTrain);
        Movable logTrain = (Movable)Proxy.newProxyInstance(tClass.getClassLoader(), tClass.getInterfaces(), logHandler);

        logTrain.move();*/

        //先记录时间，再记录日志
        Train train = new Train();
        Class<?> tClass = train.getClass();

        LogHandler logHandler = new LogHandler(train);
        Movable logTrain = (Movable)Proxy.newProxyInstance(tClass.getClassLoader(), tClass.getInterfaces(), logHandler);

        TimeHandler timeHandler = new TimeHandler(logTrain);
        Movable timeTrain = (Movable)Proxy.newProxyInstance(tClass.getClassLoader(), tClass.getInterfaces(), timeHandler);

        timeTrain.move();
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.carDriver();
//        client.trainDriver();
    }
}
