package cn.anquing.pattern.proxyPattern.jdkProxy.demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TimeHandler implements InvocationHandler {

    Object target;//目标对象

    public TimeHandler(Object target) {
        this.target = target;
    }

    /**
     *
     * @param proxy 代理对象
     * @param method 目标对象的方法
     * @param args 方法参数
     * @return 方法的返回值
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long startTime = System.currentTimeMillis();
        System.out.println("开始行驶...");
        method.invoke(target);
        long endTime = System.currentTimeMillis();
        System.out.println("结束行驶... 总耗时"+(endTime-startTime));
        return null;
    }
}
