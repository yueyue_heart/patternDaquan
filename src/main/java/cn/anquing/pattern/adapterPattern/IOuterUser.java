package cn.anquing.pattern.adapterPattern;

import java.util.Map;

public interface IOuterUser {

    //基本信息，比如名称，性别，手机号码了等
    Map<String,String> getUserBaseInfo();

    //工作区域信息
    Map<String,String> getUserOfficeInfo();

    //用户的家庭信息
    Map<String,String> getUserHomeInfo();
}
