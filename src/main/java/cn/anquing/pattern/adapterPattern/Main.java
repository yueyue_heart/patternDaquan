package cn.anquing.pattern.adapterPattern;

/**
 * 适配器模式分为类适配器和对象适配器，这个区别不大，这里的例子就是类适配器（使用了继承），
 * 那对象适配器是什么样子呢？对象适配器（使用了聚合）只需要将OuterUserInfo聚合OuterUser
 */

public class Main {

    public static void main(String[] args) {

        //没有与外系统连接的时候，是这样写的
        //IUserInfo youngGirl = new UserInfo();

        //老板一想不对呀，兔子不吃窝边草，还是找人力资源的员工好点
        IUserInfo youngGirl = new OuterUserInfo(); //我们只修改了这一句好

        //从数据库中查到101个
        for(int i=0;i<101;i++){
            youngGirl.getMobileNumber();
        }
    }
}
