package cn.anquing.pattern.mementoPattern.polymorphicDemo.caretaker;


import cn.anquing.pattern.mementoPattern.polymorphicDemo.memento.Memento;

/**
 * Caretaker备忘录管理员角色
 */
public class Caretaker {

    //备忘录对象
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }


}
