package cn.anquing.pattern.mementoPattern.simpleDemo;



/*
* 备忘录模式提供了一种弥补真实世界缺陷的方法， 让“后悔药”在程序的世界中真实可行， 其定义如下：
Without violating encapsulation,capture and externalize an object's internal state so that theobject can be restored to this state later.
（在不破坏封装性的前提下， 捕获一个对象的内部状态， 并在该对象之外保存这个状态。 这样以后就可将该对象恢复到原先保存的状态。 ）
*/

/**
 * 注意事项:
 * 备忘录的生命期
 * 备忘录的性能
 */

/**
 * 使用场景:
 * 需要保存和恢复数据的相关状态场景。
 * 提供一个可回滚（rollback） 的操作；
 * 需要监控的副本场景中。
 * 数据库连接的事务管理就是用的备忘录模式，
 */

import cn.anquing.pattern.mementoPattern.simpleDemo.caretaker.Caretaker;
import cn.anquing.pattern.mementoPattern.simpleDemo.originator.Boy;

/**
* 要考虑的是如何保留一个原始，如何恢复一个原始状态才是最重要的
 */

public class Main {

    public static void main(String[] args) {

        //声明出主角
        Boy boy = new Boy();

        //声明出备忘录的管理者
        Caretaker caretaker = new Caretaker();

        //初始化当前状态
        boy.setState("心情很棒！ ");
        System.out.println("=====男孩现在的状态======");
        System.out.println(boy.getState());

        //需要记录下当前状态呀
        caretaker.setMemento(boy.createMemento());

        //男孩去追女孩， 状态改变
        boy.changeState();
        System.out.println("\n=====男孩追女孩子后的状态======");
        System.out.println(boy.getState());

        //追女孩失败， 恢复原状
        boy.restoreMemento(caretaker.getMemento());
        System.out.println("\n=====男孩恢复后的状态======");
        System.out.println(boy.getState());

    }


}
