package cn.anquing.pattern.mementoPattern.extendDemo;



import cn.anquing.pattern.mementoPattern.extendDemo.originator.Originator;

/**
*  精简掉不必要的角色：
 *
 * 起初，备忘录设计模式是说：在不破坏封装性的前提下， 捕获一个对象的内部状态， 并在该对象之外保存这个状态。 这样以后就可将该对象恢复到原先保存的状态
 *是因为他没有预见到计模式定义的诞生比Java的出世略早， 它没有想到Java程序是这么有活力和有远见，面向对象的设计中， 即使把一个类封装在另一个类中也是可以做到的， 何况一个小小的对象复制
 */

public class Main {

    public static void main(String[] args) {

        //定义发起人
        Originator originator = new Originator();

        //建立初始状态
        originator.setState("初始状态...");
        System.out.println("初始状态是： "+originator.getState());

        //建立备份
        originator.createMemento();

        //修改状态
        originator.setState("修改后的状态...");
        System.out.println("修改后状态是： "+originator.getState());

        //恢复原有状态
        originator.restoreMemento();
        System.out.println("恢复后状态是： "+originator.getState());

    }


}
