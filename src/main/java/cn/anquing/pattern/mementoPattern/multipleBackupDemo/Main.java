package cn.anquing.pattern.mementoPattern.multipleBackupDemo;



/*
* 备忘录模式提供了一种弥补真实世界缺陷的方法， 让“后悔药”在程序的世界中真实可行， 其定义如下：
Without violating encapsulation,capture and externalize an object's internal state so that theobject can be restored to this state later.
（在不破坏封装性的前提下， 捕获一个对象的内部状态， 并在该对象之外保存这个状态。 这样以后就可将该对象恢复到原先保存的状态。 ）
*/

/**
 * 注意事项:
 * 备忘录的生命期
 * 备忘录的性能
 */

/**
 * 使用场景:
 * 需要保存和恢复数据的相关状态场景。
 * 提供一个可回滚（rollback） 的操作；
 * 需要监控的副本场景中。
 * 数据库连接的事务管理就是用的备忘录模式，
 */

import cn.anquing.pattern.mementoPattern.multipleBackupDemo.caretaker.Caretaker;
import cn.anquing.pattern.mementoPattern.multipleBackupDemo.originator.Originator;

/**
* 要考虑的是如何保留一个原始，如何恢复一个原始状态才是最重要的
 */

public class Main {

    public static void main(String[] args) {

        //定义出发起人
        Originator ori = new Originator();

        //定义出备忘录管理员
        Caretaker caretaker = new Caretaker();
        //初始化
        ori.setState1("中国");
        ori.setState2("强盛");
        ori.setState3("繁荣");
        System.out.println("===初始化状态===\n"+ori);

        //创建两个备忘录
        caretaker.setMemento("001",ori.createMemento());
        caretaker.setMemento("002",ori.createMemento());


        //修改状态值
        ori.setState1("软件");
        ori.setState2("架构");
        ori.setState3("优秀");
        System.out.println("\n===修改后状态===\n"+ori);

        //创建第三个备忘录
        caretaker.setMemento("003",ori.createMemento());


        //恢复一个指定标记的备忘录
        ori.restoreMemento(caretaker.getMemento("001"));
        System.out.println("\n===恢复后001状态===\n"+ori);

        //恢复一个指定标记的备忘录
        ori.restoreMemento(caretaker.getMemento("003"));
        System.out.println("\n===恢复后003状态===\n"+ori);
    }

}
