package cn.anquing.pattern.mementoPattern.multipleBackupDemo.caretaker;


import cn.anquing.pattern.mementoPattern.multipleBackupDemo.memento.Memento;

import java.util.HashMap;

/**
 * Caretaker备忘录管理员角色
 */
public class Caretaker {

    //容纳备忘录的容器
    private HashMap<String,Memento> memMap = new HashMap<String,Memento>();
    public Memento getMemento(String idx) {
        return memMap.get(idx);
    }
    public void setMemento(String idx,Memento memento) {
        this.memMap.put(idx, memento);
    }


}
