package cn.anquing.pattern.buildPattern.model;

public class BMWModel extends CarMoldel {

    @Override
    protected void start() {

        System.out.println("========宝马启动===========");

    }

    @Override
    protected void stop() {

        System.out.println("========宝马停止===========");

    }

    @Override
    protected void alarm() {

        System.out.println("========宝马响喇叭===========");

    }

    @Override
    protected void engineBoom() {

        System.out.println("========宝马发动机声音===========");

    }
}
