package cn.anquing.pattern.buildPattern.model;

public class BenzModel extends CarMoldel {

    @Override
    protected void start() {

        System.out.println("========奔驰启动===========");

    }

    @Override
    protected void stop() {

        System.out.println("========奔驰停止===========");

    }

    @Override
    protected void alarm() {

        System.out.println("========奔驰响喇叭===========");

    }

    @Override
    protected void engineBoom() {

        System.out.println("========奔驰发动机声音===========");

    }
}
