package cn.anquing.pattern.buildPattern.model;

import java.util.ArrayList;
import java.util.List;

public abstract class CarMoldel {

    private List<String> sequence = new ArrayList<String>();


    protected abstract void start();

    protected abstract void stop();

    protected abstract void alarm();

    protected abstract void engineBoom();

    final public void run(){

        //循环一遍，谁在前，就先执行谁
        for(int i=0;i<this.sequence.size();i++){
            String actionName = this.sequence.get(i);
            if(actionName.equalsIgnoreCase("start")){ //如果是
                this.start(); //开启汽车
            }else if(actionName.equalsIgnoreCase("stop")){ //
                this.stop(); //停止汽车
            }else if(actionName.equalsIgnoreCase("alarm")){
                this.alarm(); //喇叭开始叫了
            }else if(actionName.equalsIgnoreCase("engineBoom")){
                this.engineBoom(); //引擎开始轰鸣
            }
        }

    }

    final public void setSequence(List<String> sequence){
        this.sequence = sequence;
    }
}
