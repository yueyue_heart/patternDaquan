package cn.anquing.pattern.buildPattern.director;

import cn.anquing.pattern.buildPattern.builder.BMWBuilder;
import cn.anquing.pattern.buildPattern.builder.BenzBuilder;
import cn.anquing.pattern.buildPattern.model.BMWModel;
import cn.anquing.pattern.buildPattern.model.BenzModel;

import java.util.Arrays;
import java.util.List;

public class Director {

    public BenzModel getABenzModel(){

        BenzBuilder benzBuilder = new BenzBuilder();

        List<String> sequence = Arrays.asList("start", "stop");

        benzBuilder.setSequence(sequence);

        return (BenzModel) benzBuilder.getCarModel();
    }

    public BenzModel getBBenzModel(){

        BenzBuilder benzBuilder = new BenzBuilder();

        List<String> sequence = Arrays.asList("start","alarm","engineBoom","stop");

        benzBuilder.setSequence(sequence);

        return (BenzModel) benzBuilder.getCarModel();
    }

    public BMWModel getCBMWModel(){

        BMWBuilder bMWBuilder = new BMWBuilder();

        List<String> sequence = Arrays.asList("start", "stop");

        bMWBuilder.setSequence(sequence);

        return (BMWModel) bMWBuilder.getCarModel();
    }

    public BMWModel getDBMWModel(){
        BMWBuilder bMWBuilder = new BMWBuilder();

        List<String> sequence = Arrays.asList("start","alarm","engineBoom","stop");

        bMWBuilder.setSequence(sequence);

        return (BMWModel) bMWBuilder.getCarModel();
    }
}
