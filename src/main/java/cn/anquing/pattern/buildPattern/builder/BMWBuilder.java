package cn.anquing.pattern.buildPattern.builder;

import cn.anquing.pattern.buildPattern.model.BMWModel;
import cn.anquing.pattern.buildPattern.model.CarMoldel;

import java.util.List;

public class BMWBuilder extends CarBuilder {

    private BMWModel bMWModel = new BMWModel();

    @Override
    public void setSequence(List<String> sequence) {
        bMWModel.setSequence(sequence);
    }

    @Override
    public CarMoldel getCarModel() {
        return this.bMWModel;
    }
}
