package cn.anquing.pattern.buildPattern.builder;

import cn.anquing.pattern.buildPattern.model.BenzModel;
import cn.anquing.pattern.buildPattern.model.CarMoldel;

import java.util.List;

public class BenzBuilder extends CarBuilder {

    private BenzModel benzModel = new BenzModel();


    @Override
    public void setSequence(List<String> sequence) {
        benzModel.setSequence(sequence);
    }

    @Override
    public CarMoldel getCarModel() {
        return this.benzModel;
    }
}
