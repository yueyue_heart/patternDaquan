package cn.anquing.pattern.buildPattern.builder;

import cn.anquing.pattern.buildPattern.model.CarMoldel;

import java.util.List;

public abstract class CarBuilder {

    public abstract void setSequence(List<String> sequence);

    public abstract CarMoldel  getCarModel();
}
