package cn.anquing.pattern.buildPattern;

import cn.anquing.pattern.buildPattern.director.Director;
import cn.anquing.pattern.buildPattern.model.BMWModel;
import cn.anquing.pattern.buildPattern.model.BenzModel;

/*
* 建造者模式
*/

/*
最主要功能是基本方法的调用顺序安排，也就是这些基本方
法已经实现了；而工厂方法则重点是创建，你要什么对象我创造一个对象出来，组装顺序则不是他关心的。
建造者模式使用的场景，一是产品类非常的复杂，或者产品类中的调用顺序不同产生了不同的效能，这个时
候使用建造者模式是非常合适，我曾在一个银行交易类项目中遇到了这个问题，一个产品的定价计算模型有 N 多
种，每个模型有固定的计算步骤，计算非常复杂，项目中就使用了建造者模式；二是“在对象创建过程中会使
用到系统中的一些其它对象，这些对象在产品对象的创建过程中不易得到”，这个是我没有遇到过的，创建过程
中不易得到？那为什么在设计阶段不修正这个问题，创建的时候都不易得到耶！
*/

/**
 * 模式讲解：

 指挥者（Director）直接和客户（Client）进行需求沟通；
 沟通后指挥者将客户创建产品的需求划分为各个部件的建造请求（Builder）；
 将各个部件的建造请求委派到具体的建造者（ConcreteBuilder）；
 各个具体建造者负责进行产品部件的构建；
 最终构建成具体产品（Product）。
 */
public class Main {

    public static void main(String[] args) {

        Director director = new Director();

        BenzModel aBenzModel = director.getABenzModel();
        aBenzModel.run();


        BenzModel bBenzModel = director.getBBenzModel();
        bBenzModel.run();

        BMWModel cbmwModel = director.getCBMWModel();
        cbmwModel.run();

        BMWModel dbmwModel = director.getDBMWModel();
        dbmwModel.run();
    }



}
