package cn.anquing.pattern.chainofResponsibilityPattern.type1;

import java.util.ArrayList;
import java.util.Random;

/**
 * 比较常规的一个责任链模式，实现类要逐一设置相互依赖，藕合度高
 */
public class Client {

    public static void main(String[] args) {

        //随机挑选几个女性
        Random rand = new Random();
        ArrayList<IWomen> arrayList = new ArrayList();
        for (int i = 0; i < 5; i++) {
            arrayList.add(new Women(rand.nextInt(4), "我要出去逛街"));
        }
        //定义三个请示对象
        Handler father = new Father();
        Handler husband = new Husband();
        Handler son = new Son();

        //设置请示顺序
        father.setNext(husband);
        husband.setNext(son);
        for (IWomen women : arrayList) {
            father.handleMessage(women);
        }
    }
}
