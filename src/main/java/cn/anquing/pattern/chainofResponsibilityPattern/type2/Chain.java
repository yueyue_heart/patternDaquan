package cn.anquing.pattern.chainofResponsibilityPattern.type2;


import java.util.List;

public class Chain {

    private int index = 0;

    List<ChainHandler> chainHandlers;

    public Chain(List<ChainHandler> chainHandlers) {
        this.chainHandlers = chainHandlers;
    }
    public void proceed(IWomen women){

        if(index>=chainHandlers.size()){
            return;
        }
        chainHandlers.get(index++).excute(this,women);
    }

}
