package cn.anquing.pattern.chainofResponsibilityPattern.type2;

public abstract class ChainHandler {

    //能处理的级别
    private int level =0;


    //每个类都要说明一下自己能处理哪些请求
    public ChainHandler(int _level){
        this.level = _level;
    }

    public void excute(Chain chain,IWomen women){
        if(level == women.getType()){
            response(women);
        }
        chain.proceed(women);
    }

    //有请示那当然要回应
    public abstract void response(IWomen women);




}
