package cn.anquing.pattern.chainofResponsibilityPattern.type2;


public class Father extends ChainHandler {


    public Father() {
        super(1);
    }

    //父亲的答复
    @Override
    public void response(IWomen women) {
        System.out.println("--------女儿向父亲请示-------");
        System.out.println(women.getRequest());
        System.out.println("父亲的答复是:同意\n");
    }
}
