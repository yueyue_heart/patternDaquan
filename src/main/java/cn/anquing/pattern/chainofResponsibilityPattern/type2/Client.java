package cn.anquing.pattern.chainofResponsibilityPattern.type2;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * spring 多个aop 就是使用这个链式模式，实现类之间解藕
 */
public class Client {

    public static void main(String[] args) {

        //随机挑选几个女性
        Random rand = new Random();
        ArrayList<IWomen> arrayList = new ArrayList();
        for (int i = 0; i < 5; i++) {
            arrayList.add(new Women(rand.nextInt(2)+1, "我要出去逛街"));
        }

        //定义三个请示对象
        ChainHandler father = new Father();
        ChainHandler husband = new Husband();
        ChainHandler son = new Son();
        //设置请示顺序
        List<ChainHandler> chainHandlers = Arrays.asList(father,husband,son);


        for (IWomen women : arrayList) {
            Chain chain = new Chain(chainHandlers);
            chain.proceed(women);
        }
    }
}
