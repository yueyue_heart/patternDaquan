package cn.anquing.pattern.flyweightPattern;

/*
* 亨元模式
*享元模式（Flyweight Pattern） 是池技术的重要实现方式， 其定义如下： Use sharing to support large numbers of fine-grained objects efficiently.（使用共享对象可有效地支持大量的细粒度的对象。 ）
*/
/**
 *
 * 要求细粒度对象， 那么不可避免地使得对象数量多且性质相近， 那我们就将这些对象的
 信息分为两个部分： 内部状态（intrinsic） 与外部状态（extrinsic） 。
 ● 内部状态
 内部状态是对象可共享出来的信息， 存储在享元对象内部并且不会随环境改变而改变，
 如我们例子中的id、 postAddress等， 它们可以作为一个对象的动态附加信息， 不必直接储存
 在具体某个对象中， 属于可以共享的部分。
 ● 外部状态
 外部状态是对象得以依赖的一个标记， 是随环境改变而改变的、 不可以共享的状态， 如
 我们例子中的考试科目+考试地点复合字符串， 它是一批对象的统一标识， 是唯一的一个索引值。
 外部状态最好以Java的基本
 类型作为标志， 如String、 int等， 可以大幅地提升效率
 */

/**
 * 享元模式是一个非常简单的模式， 它可以大大减少应用程序创建的对象， 降低程序内存
 的占用， 增强程序的性能， 但它同时也提高了系统复杂性， 需要分离出外部状态和内部状
 态， 而且外部状态具有固化特性， 不应该随内部状态改变而改变， 否则导致系统的逻辑混
 乱
 */

/**
 对象池（Object Pool） 的实现有很多开源工具， 比如Apache的commons-pool就是一个非常不错的池工具，
 我们暂时还用不到这种重量级的工具， 我们自己来设计一个共 享对象池， 需要实现如下两个功能:容器定义,提供客户端访问的接口
 */

public class Main {

    public static void main(String[] args) {
        //初始化对象池
        for(int i=0;i<4;i++){
            String subject = "科目" + i;
        //初始化地址
            for(int j=0;j<30;j++){
                String key = subject + "考试地点"+j;
                SignInfoFactory.getSignInfo(key);
            }
        }

        SignInfo signInfo = SignInfoFactory.getSignInfo("科目1考试地点1");
    }

}
