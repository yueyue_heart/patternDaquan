package cn.anquing.pattern.iteratorPattern.Iterator;

import cn.anquing.pattern.iteratorPattern.Project.IProject;

import java.util.ArrayList;

public class ProjectIterator implements IProjectIterator {

    //所有的项目都放在这里ArrayList中
    private ArrayList<IProject> projectList = new ArrayList<IProject>();


    public ProjectIterator(ArrayList<IProject> projectList) {
        this.projectList = projectList;
    }

    private int currentItem = 0;

    @Override
    public boolean hasNext() {

        //定义一个返回值
        boolean b = true;
        if(this.currentItem>=projectList.size() || this.projectList.get(this.currentItem) == null){
            b =false;
        }
        return b;
    }


    @Override
    public IProject next() {
        return (IProject)this.projectList.get(this.currentItem++);
    }


}
