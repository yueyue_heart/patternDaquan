package cn.anquing.pattern.iteratorPattern;

/*
* 迭代器模式
*/

import cn.anquing.pattern.iteratorPattern.Iterator.IProjectIterator;
import cn.anquing.pattern.iteratorPattern.Project.IProject;
import cn.anquing.pattern.iteratorPattern.Project.Project;

/**
 *基本上现在所有的高级的语言都有 Iterator 这个接口或者实现， Java 已经把迭代器给我们准备了
 * 这个迭代器模式也有点没落了，基本上很少有项目再独立写迭代器了，直接使用 List 或者 Map 就可以完整的解决问题
 */

public class Main {

    public static void main(String[] args) {

        //定义一个List，存放所有的项目对象
        IProject project = new Project();
        //增加星球大战项目
        project.add("星球大战项目ddddd",10,100000);
        //增加扭转时空项目
        project.add("扭转时空项目",100,10000000);
        //增加超人改造项目
        project.add("超人改造项目",10000,1000000000);
        //这边100个项目
        for(int i=4;i<104;i++){
        project.add("第"+i+"个项目",i*5,i*1000000);
        }

        //遍历一下ArrayList，把所有的数据都取出
        IProjectIterator projectIterator = project.iterator();

        while(projectIterator.hasNext()){
            IProject p = (IProject)projectIterator.next();
            System.out.println(p.getProjectInfo());
        }

    }


}
